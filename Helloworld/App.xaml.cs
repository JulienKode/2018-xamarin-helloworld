﻿using System.Diagnostics;
using Xamarin.Forms;

namespace Helloworld
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new HelloworldPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            Debug.WriteLine("OnStart was call");
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Debug.WriteLine("OnSleep was call");
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            Debug.WriteLine("OnResume was call");
        }
    }
}
