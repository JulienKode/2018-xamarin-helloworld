﻿using System;
using Xamarin.Forms;

namespace Helloworld
{
    public partial class HelloworldPage : ContentPage
    {
        int clickTotal = 0;

        public HelloworldPage()
        {
            InitializeComponent();
        }

        void NativeButtonClick(object sender, System.EventArgs e)
        {
            topLabel.Text = "Xamarin is really reactive!";
        }

        void CounterClick(object sender, System.EventArgs e)
        {
            clickTotal += 1;
            counterButton.Text = String.Format("{0} button click{1}",
                                          clickTotal, clickTotal == 1 ? "" : "s");
        }
    }
}
